/*..
....
ARRAYS
....
..*/

const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

let RespostaKatas = [katas1, katas2, katas3, katas4, katas5, katas6, katas7, katas8, katas9, katas10, katas11, katas12, katas13, katas14, katas15, katas16, katas17, katas18];

/*..
....
FUNÇÕES
....
..*/

let meu_carro = {
    nome: "Civic",
    marca: "Honda",
    ano: 2015,
    ligar: function() {
        console.log("vrrrummmm")
    }
}

meu_carro.ligar()

function katas1 () {
    let resultado = ""
    for (let a = 1; a < 25; a++) {
        resultado = resultado + a + ", ";
        }
    return resultado + "25."
}

console.log(katas1())
let ResultadoKatas1 = document.createElement('p')
ResultadoKatas1.innerHTML = katas1()
document.getElementById( "div1").appendChild(ResultadoKatas1)

function katas2 () {
    let resultado = ""
    for (let a = 25; a > 1; a--) {
        resultado = resultado + a + ", ";
        }
    return resultado + "1."
}

console.log (katas2())
let ResultadoKatas2 = document.createElement('p')
ResultadoKatas2.innerHTML = katas2()
document.getElementById( "div2").appendChild(ResultadoKatas2)

function katas3 () {
    let resultado = ""
    for (let a = -1; a > -25; a--) {
        resultado = resultado + a + ", ";
        }
    return resultado + "-25."
    
}

console.log (katas3())
let ResultadoKatas3 = document.createElement('p')
ResultadoKatas3.innerHTML = katas3()
document.getElementById( "div3").appendChild(ResultadoKatas3)

function katas4 () {
    let resultado = ""
    for (let a = -25; a < -1; a++) {
        resultado = resultado + a + ", ";
        }
    return resultado + "-1."
}

console.log (katas4())
let ResultadoKatas4 = document.createElement('p')
ResultadoKatas4.innerHTML = katas4()
document.getElementById( "div4").appendChild(ResultadoKatas4)

function katas5 () {
    let resultado = ""
    for (let a = 25; a > -25; a = a - 2) {
        resultado = resultado + a + ", ";
        }
    return resultado + "-25."
}

console.log (katas5())
let ResultadoKatas5 = document.createElement('p')
ResultadoKatas5.innerHTML = katas5()
document.getElementById( "div5").appendChild(ResultadoKatas5)


function katas6 () {
    let resultado = ""
    for (let a = 3; a < 99; a = a + 3) {
        resultado = resultado + a + ", ";
        }
    return resultado + "99."
}

console.log (katas6())
let ResultadoKatas6 = document.createElement('p')
ResultadoKatas6.innerHTML = katas6()
document.getElementById( "div6").appendChild(ResultadoKatas6)

function katas7 () {
    let resultado = ""
    for (let a = 7; a < 98; a = a + 7) {
        resultado = resultado + a + ", ";
        }
    return resultado + "98."
}

console.log (katas7())
let ResultadoKatas7 = document.createElement('p')
ResultadoKatas7.innerHTML = katas7()
document.getElementById( "div7").appendChild(ResultadoKatas7)

function katas8 () {
    let resultado = ""
    for (let a = 99; a > 3; a--) {
        if( a % 3 == 0 || a % 7 == 0) {
        resultado = resultado + a + ", ";
        }
    }
    return resultado + "3."
}

console.log (katas8())
let ResultadoKatas8 = document.createElement('p')
ResultadoKatas8.innerHTML = katas8()
document.getElementById( "div8").appendChild(ResultadoKatas8)

function katas9 () {
    let resultado = ""
    for (let b = 5; b < 95; b = b + 5) {
        if( b % 2 == 1) {
        resultado = resultado + b + ", ";
        }
    }
    return resultado + "95."
}

console.log (katas9())
let ResultadoKatas9 = document.createElement('p')
ResultadoKatas9.innerHTML = katas9()
document.getElementById( "div9").appendChild(ResultadoKatas9)

function katas10 () {
    let resultado = ""
    for (let a = 0; a < 19; a++) {
        resultado = resultado + sampleArray[a] + ", "
    }
    return resultado + "472."
}

console.log (katas10())
let ResultadoKatas10 = document.createElement('p')
ResultadoKatas10.innerHTML = katas10()
document.getElementById( "div10").appendChild(ResultadoKatas10)

function katas11 () {
    let resultado = ""
      for (let a = 0; a < 19; a++) {
        if( sampleArray[a] % 2 == 0) {
          resultado = resultado + sampleArray[a] + ", "
        }
      }
      return resultado + "472."
}

console.log (katas11())
let ResultadoKatas11 = document.createElement('p')
ResultadoKatas11.innerHTML = katas11()
document.getElementById( "div11").appendChild(ResultadoKatas11)

function katas12 () {  
    let resultado = ""
      for (let a = 0; a < 17; a++) {
        if( sampleArray[a] % 2 != 0) {
          resultado = resultado + sampleArray[a] + ", "
        }
      }
      return resultado + "535."
}

console.log (katas12())
let ResultadoKatas12 = document.createElement('p')
ResultadoKatas12.innerHTML = katas12()
document.getElementById( "div12").appendChild(ResultadoKatas12)

function katas13 () {  
    let resultado = ""
      for (let a = 0; a < 20; a++) {
        if( sampleArray[a] % 8 == 0) {
          resultado = resultado + sampleArray[a] + ", "
        }
      }
      return resultado + "."
}

console.log (katas13())
let ResultadoKatas13 = document.createElement('p')
ResultadoKatas13.innerHTML = katas13()
document.getElementById( "div13").appendChild(ResultadoKatas13)

function katas14 () {  
    let resultado = ""
      for (let a = 0; a < 20; a++) {
          resultado = resultado + (sampleArray[a]**2) + ", "
        }
      return resultado + "."
}

console.log (katas14())
let ResultadoKatas14 = document.createElement('p')
ResultadoKatas14.innerHTML = katas14()
document.getElementById( "div14").appendChild(ResultadoKatas14)

function katas15 () {  
    let resultado = 0
      for (let a = 0; a <= 20; a++) {
          if (a <= 20) {
              resultado = resultado + a;
          }
        }
      return resultado + "."
}

console.log (katas15())
let ResultadoKatas15 = document.createElement('p')
ResultadoKatas15.innerHTML = katas15()
document.getElementById( "div15").appendChild(ResultadoKatas15)

function katas16 () {  
    let resultado = 0
      for (let a = 0; a < 20; a++) {
          if (a < 20) {
              resultado = resultado + sampleArray[a];
          }
        }
      return resultado + "."
}

console.log (katas16())
let ResultadoKatas16 = document.createElement('p')
ResultadoKatas16.innerHTML = katas16()
document.getElementById( "div16").appendChild(ResultadoKatas16)

function katas17() {
    let resultado = 0
    resultado = Math.min(...sampleArray)
    return resultado
}

console.log (katas17())
let ResultadoKatas17 = document.createElement('p')
ResultadoKatas17.innerHTML = katas17()
document.getElementById( "div17").appendChild(ResultadoKatas17)

function katas18() {
    let resultado = 0
    resultado = Math.max(...sampleArray)
    return resultado
}

console.log (katas18())
let ResultadoKatas18 = document.createElement('p')
ResultadoKatas18.innerHTML = katas18()
document.getElementById( "div18").appendChild(ResultadoKatas18)